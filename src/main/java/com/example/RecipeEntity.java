package com.example;
import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="Recette")
public class RecipeEntity {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    private String nomRecette;

    private static final long serialVersionUID = 1L;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomRecette() {
        return nomRecette;
    }

    public void setNomRecette(String nomRecette) {
        this.nomRecette = nomRecette;
    }

    @Override
    public String toString()
    {
        return "id: " + Integer.toString(id) + " , name: " + nomRecette;
    }
}
