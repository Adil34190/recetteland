package com.example;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class PersistenceManagerBase {
    protected static final EntityManager em = HibernateHelper.getSession().getEntityManagerFactory().createEntityManager();

    public boolean persist(RecipeEntity toPersist) {
        if (toPersist == null)
            return false;
        this.em.getTransaction().begin();
        this.em.persist(toPersist);
        this.em.getTransaction().commit();
        return true;
    }

    public List<RecipeEntity> getAll() {
        return this.em.createQuery("SELECT r FROM RecipeEntity r").getResultList();
    }

    public RecipeEntity getOne(String id) {
        List<RecipeEntity> recettes =  this.em.createQuery("SELECT r FROM RecipeEntity r WHERE id=" + id).getResultList();
        return recettes.get(0);
    }
}
