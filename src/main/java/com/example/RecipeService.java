package com.example;

import org.apache.log4j.Logger;

import javax.persistence.*;
import java.util.List;

public class RecipeService extends PersistenceManagerBase{
    private Logger logger = Logger.getLogger(RecipeService.class);

    public List<RecipeEntity> findAll()
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("RecettePU");
        EntityManager em = emf.createEntityManager();

        logger.info("EMF created");

        Query query = em.createQuery("from Recette", RecipeEntity.class);
        List<RecipeEntity> recettes = query.getResultList();

        for(RecipeEntity r : recettes)
        {
            logger.info("Recette récupérée: " + r.getNomRecette());
        }

        em.close();
        emf.close();

        return recettes;

    }

    public RecipeEntity findById(final int id)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("RecettePU");
        EntityManager em = emf.createEntityManager();

        logger.info("EMF created");

        RecipeEntity recetteRecuperee = em.find(RecipeEntity.class, id);
        logger.info("Recette récupérée par id: " + recetteRecuperee.getNomRecette());

        em.close();
        emf.close();

        return recetteRecuperee;

    }

    public void createRecipe(RecipeEntity recipe) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("RecettePU");
        EntityManager em = emf.createEntityManager();

        logger.info("EMF created");

        EntityTransaction transaction = em.getTransaction();
        transaction.begin();

        em.persist(recipe);
        transaction.commit();
        logger.info("Recette ajoutée");
        em.close();
        emf.close();
    }
}
