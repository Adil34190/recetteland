package com.example;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import java.util.List;

public class VerticleApi extends AbstractVerticle {
    private Logger logger = Logger.getLogger(VerticleApi.class);
    private final RecipeService recipeService = new RecipeService();
    //lancement du verticle
    @Override
    public void start() throws Exception {
        logger.info("On start");
        logger.error("Test erreur");
        // Création du routeur
        Router router = Router.router(vertx);
        // Body handler
        router.route("/api/recipes*").handler(BodyHandler.create());
        // Définition de la route
        router.get("/api/recipes")
                .handler(this::getAllRecipes);
        router.get("/api/recipes/:id")
                .handler(this::getOnerecipe);
        router.post("/api/recipes")
                .handler(this::createOneRecipe);
        // Lancement du serveur
        vertx.createHttpServer()
                .requestHandler(router)
                .listen(1010);
    }


    //arret du verticle
    @Override
    public void stop() throws Exception {
        logger.info("on stop");
        super.stop();
    }

    private void getAllRecipes(RoutingContext routingContext){
        try{
            logger.info("get all recipes");
            logger.error("Test erreur");
            // Recherche des recettes
            final List<RecipeEntity> recettes = recipeService.getAll();
            // Création et remplissage de la réponse
            final JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("recettes", recettes);
            // Envoi de la réponse
            routingContext.response()
                    .setStatusCode(200)
                    .putHeader("content-type", "application/json")
                    .end(jsonResponse.toString());
        }catch (Exception e)
        {
            logger.error(e);
        }

    }

    private  void getOnerecipe(RoutingContext routingContext) {
        try{
            logger.info("get one recipe");
            // Param id de la requête
            final String id = routingContext.request().getParam("id");
            // Recherche de la recette correspondant à l'id
            final RecipeEntity recette = recipeService.getOne(id);
            if (recette == null) {
                final JSONObject errorJsonResponse = new JSONObject();
                errorJsonResponse.put("error", "No recipe can be found for the specified id:" + id);
                errorJsonResponse.put("id", id);
                // Envoi de la réponse avec erreur 404
                routingContext.response()
                        .setStatusCode(404)
                        .putHeader("content-type", "application/json")
                        .end(errorJsonResponse.toString());
                return;
            }
            // Envoi de la réponse
            routingContext.response()
                    .setStatusCode(200)
                    .putHeader("content-type", "application/json")
                    .end(recette.getNomRecette());

        }catch (Exception e)
        {
            logger.error(e);
        }


    }


    private void createOneRecipe(RoutingContext routingContext) {
        logger.info("create one recipe");
        final JsonObject body = routingContext.getBodyAsJson();
        final String name = body.getString("name");
        final RecipeEntity recette = new RecipeEntity();
        recette.setNomRecette(name);
        recipeService.persist(recette);
        // Envoi de la réponse
        routingContext.response()
                .setStatusCode(201)
                .putHeader("content-type", "application/json")
                .end("Recette créé");
    }
}
